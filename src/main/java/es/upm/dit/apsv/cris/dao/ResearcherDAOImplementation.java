/**
 * 
 */
package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

/**
 * @author ecarr
 * @version 1.0
 *
 */
public class ResearcherDAOImplementation implements ResearcherDAO {
	
	//Para que solo pueda haber un objeto en memoria
	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {}
	public static ResearcherDAOImplementation getInstance() {
		if (null == instance)
			instance = new ResearcherDAOImplementation();
		return instance;
	}
	
	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
			//abrir una transaccion
			session.beginTransaction();
			session.save(researcher);
			session.getTransaction().commit();
		}catch (Exception e) {
		}finally {	//cierra la sesion
			session.close();
		}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Session session = SessionFactoryService.get().openSession();
		Researcher r = null;
		try {
			//abrir una transaccion
			session.beginTransaction();
			r = session.get(Researcher.class, researcherId);
			session.getTransaction().commit();
		}catch (Exception e) {
		}finally {	//cierra la sesion
			session.close();
		}
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
			//abrir una transaccion
			session.beginTransaction();
			session.saveOrUpdate(researcher);
			session.getTransaction().commit();
		}catch (Exception e) {
		}finally {	//cierra la sesion
			session.close();
		}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
			//abrir una transaccion
			session.beginTransaction();
			session.delete(researcher);
			session.getTransaction().commit();
		}catch (Exception e) {
		}finally {	//cierra la sesion
			session.close();
		}
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List <Researcher> l = null;
		try {
			//abrir una transaccion
			session.beginTransaction();
			l = (List <Researcher>) session.createQuery("from Researcher").getResultList();
			session.getTransaction().commit();
		}catch (Exception e) {
		}finally {	//cierra la sesion
			session.close();
		}
		return l;
	}

	@Override
	public Researcher readByEmail(String email) {
		for (Researcher r : this.readAll()) {
			if (email.equals(r.getEmail()))
				return r;
		}
		return null;
	}

}
