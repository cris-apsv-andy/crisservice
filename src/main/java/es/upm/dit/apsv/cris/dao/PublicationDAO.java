/**
 * 
 */
package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Publication;

/**
 * @author ecarr
 * @version 1.0
 *
 */
public interface PublicationDAO {
	public Publication create( Publication Publication );
	public Publication read( String PublicationId );
	public Publication update( Publication Publication );
	public Publication delete( Publication Publication );

	List<Publication> readAll();
	List<Publication> readAllPublications(String researcherId);
}
